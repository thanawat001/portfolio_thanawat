export default [
  {
    title: 'HOME',
    route: 'home',
    icon: 'UserIcon',
  },
  {
    title: 'SERVICES',
    route: 'services',
    icon: 'BookOpenIcon',
  },

  {
    title: 'PORTFOLIO',
    route: 'portfolio',
    icon: 'AwardIcon',
  },
]
